﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using System.IO;
using UnityEngine.SceneManagement;

public class Mani : MonoBehaviour
{
    public GameObject setting_page;
    public int long_at;
    public int high_at;
    public Text 長;
    public Text 高;

    public int X_at;
    public int Y_at;
    public Text 位移X;
    public Text 位移Y;
    public InputField 長_ip;
    public InputField 高_ip;
    public InputField 位移X_ip;
    public InputField 位移Y_ip;

    public nofram nofsc;

    public VideoPlayer video;
    public VideoClip clip_1;
    public VideoClip clip_2;

    private string nam;
    string file = @"C:\Scan"; //針測的資料夾;
    FileSystemWatcher watch;
    public bool born;
    public RawImage pic;

    public int check;
    public int check_1;

    public GameObject name;
    public GameObject logo;
    public Text name_text;
    public List<string> allwords;
    public int name_count = 0;

    public Dropdown word_list;
    public InputField addwd;
    public bool settingbl;

    // Start is called before the first frame update
    void Start()
    {
        //PlayerPrefs.DeleteAll();
        if (name != null)
        {
            StartCoroutine(name_play());
        }

        find_defalt();
        change_path();
        
        //StartCoroutine(waitplay());
        //Screen.SetResolution(5760, 2160, false);
        check = PlayerPrefs.GetInt("check");
        check_1 = PlayerPrefs.GetInt("check_1");

        if (check_1 == 0)
        {
            if (word_list != null)
            {
                word_list.AddOptions(allwords);
            }
        }
        if (check == 1)
        {
            long_at = PlayerPrefs.GetInt("long_at");
            high_at = PlayerPrefs.GetInt("high_at");
            X_at = PlayerPrefs.GetInt("X_at");
            Y_at = PlayerPrefs.GetInt("Y_at");
        }
        if (check_1 == 1)
        {
            if (name != null)
            {
                allwords.Clear();
                for (int y = 0; y < PlayerPrefs.GetInt("word_list"); y++)
                {
                    allwords.Add(PlayerPrefs.GetString("word" + y));
                }
            }
            if (word_list != null)
            {
                word_list.AddOptions(allwords);
            }
        }

        nofsc.screenPosition.x = X_at;
        nofsc.screenPosition.y = Y_at;

        nofsc.screenPosition.width = long_at;
        nofsc.screenPosition.height = high_at;

        長_ip.text = long_at.ToString();
        高_ip.text = high_at.ToString();
        位移X_ip.text = X_at.ToString();
        位移Y_ip.text = Y_at.ToString();

        nofsc.SetNoFrame();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            settingbl = !settingbl;
        }
        if (settingbl)
        {
            setting_page.transform.localPosition = new Vector3(0, 0, 0);
        }
        if (settingbl == false)
        {
            setting_page.transform.localPosition = new Vector3(0, 3000, 0);
        }
        if (Input.GetKeyDown(KeyCode.KeypadEnter))
        {
            check_fn();
        }
        //Screen.SetResolution(20000, 1080, false);

        if (born)
        {
            //创建文件读取流
            FileStream fs = new FileStream(nam, FileMode.Open, FileAccess.Read);
            //FileStream fs = new FileStream()
            fs.Seek(0, SeekOrigin.Begin);
            //创建文件长度缓冲区
            byte[] bytes = new byte[fs.Length];
            //读取文件
            fs.Read(bytes, 0, (int)fs.Length);
            //释放文件读取流
            fs.Close();
            fs.Dispose();
            fs = null;
            //加载图片资源
            Texture2D texture = new Texture2D(1351 , 323);
            texture.LoadImage(bytes);
            //为材质加载图片  
            pic.texture = texture;
            if (logo != null)
            {
                logo.GetComponent<RawImage>().texture = texture;
            }

            born = false;
        }
        
    }
    IEnumerator waitplay()
    {
        yield return new WaitForSeconds(2f);
    }
    public void check_fn()
    {
        long_at = int.Parse(長.text);
        high_at = int.Parse(高.text);

        X_at = int.Parse(位移X.text);
        Y_at = int.Parse(位移Y.text);

        nofsc.screenPosition.x = X_at;
        nofsc.screenPosition.y = Y_at;

        nofsc.screenPosition.width = long_at;
        nofsc.screenPosition.height = high_at;

        nofsc.SetNoFrame();

        PlayerPrefs.SetInt("long_at", long_at);
        PlayerPrefs.SetInt("high_at", high_at);
        PlayerPrefs.SetInt("X_at", X_at);
        PlayerPrefs.SetInt("Y_at", Y_at);
        PlayerPrefs.SetInt("start", Y_at);

        check = 1;
        check_1 = 1;
        PlayerPrefs.SetInt("check", check);
        PlayerPrefs.SetInt("check_1", check_1);
        settingbl = false;
    }
    public void quit_fn()
    {
        Application.Quit();
    }
    public void switch_fn(int xx)
    {
        if (xx == 0)
        {
            video.clip = clip_1;
        }
        if (xx == 1)
        {
            video.clip = clip_2;
        }
    }
    public void change_path()
    {
        watch = new FileSystemWatcher(file, "*.png");

        //開啟監聽
        watch.EnableRaisingEvents = true;

        //新增時觸發事件
        watch.Created += listen;
    }
    public void find_defalt()
    {
        DirectoryInfo direction = new DirectoryInfo(file);
        FileInfo[] files = direction.GetFiles("*.png", SearchOption.AllDirectories);
        if (files.Length != 0)
        {
            nam = files[files.Length - 1].FullName;
            Debug.Log(nam);
            born = true;
        }
    }
    void listen(object sender, FileSystemEventArgs e)
    {
        born = true;
        nam = e.Name;
    }
    public void picswitch_fn()
    {
        pic.enabled = !pic.enabled;
    }
    public void nameplay_fn()
    {
        if (name_count < allwords.Count)
        {
            name_text.text = allwords[name_count];
            StartCoroutine(wait_play());
        }
    }
    public void addword_fn()
    {
        if (addwd.text != null)
        {
            allwords.Add(addwd.text);
            word_list.ClearOptions();
            word_list.AddOptions(allwords);
        }
    }
    public void minusword_fn()
    {
        allwords.Remove(allwords[word_list.value]);
        word_list.ClearOptions();
        word_list.AddOptions(allwords);
        word_list.value = 0;
        addwd.text = allwords[0];
    }
    public void value_fn()
    {
        addwd.text = allwords[word_list.value];
    }
    public void opChange_fn()
    {
        allwords[word_list.value] = addwd.text;
        word_list.ClearOptions();
        word_list.AddOptions(allwords);
    }
    public void OnApplicationQuit()
    {
        PlayerPrefs.SetInt("word_list", allwords.Count);
        for (int x = 0; x < allwords.Count;x++)
        {
            PlayerPrefs.SetString("word"+x, allwords[x]);
        }
        Application.Quit();
    }
    public void reset_fn()
    {
        PlayerPrefs.DeleteKey("word_list");
        PlayerPrefs.DeleteKey("word");
    }
    IEnumerator wait_play()
    {
        name_text.GetComponent<Animator>().Play("Name");
        yield return new WaitForSeconds(3);
        name_count++;
        if (name_count == allwords.Count)
        {
            name_count = 0;
        }
        nameplay_fn();
    }
    IEnumerator name_play()
    {
        yield return new WaitForSeconds(3);
        name.SetActive(true);
        logo.SetActive(true);
        nameplay_fn();
    }
}
