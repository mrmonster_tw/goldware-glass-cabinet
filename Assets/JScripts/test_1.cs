﻿using UnityEngine;
using System.Collections;
using DIO_Control_ver2;

public class test_1 : MonoBehaviour
{
    DIO_to_unity DIO = new DIO_to_unity(); //啟用DLL CLASS

    public int read; //宣告要從電路板讀的變數

    void Start()
    {
        DIO.setup();    //打開與控制板的連接通道
    }

    void Update()
    {
        read = DIO.Receive(read); //每秒刷新讀取到的值
        if (read != 0)
        {
            print(read);
        }

        if (read == 65) //可讀取65~72
        {
            Debug.Log("65");
            DIO.LED_ON(1, 1);      //一號燈亮(共有八顆燈)
            Application.LoadLevel("main");
        }

        if (read == 66) //可讀取65~72
        {
            Debug.Log("66");
            DIO.LED_OFF(1, 1);     //一號燈熄(共有八顆燈)
            //Application.LoadLevel("customer");
        }
    }

    void OnApplicationQuit()
    {
        DIO.Close_DIO();    //關閉連接通道()
        //注意：換場景(scene)時一定要關閉通道
        //然後下個場景再用DIO.setup()函數開啟通道
    }


}